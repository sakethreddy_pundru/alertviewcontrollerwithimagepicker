//
//  ViewController.m
//  AlertViewController
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
@import MobileCoreServices;

@interface ViewController ()

@end

@implementation ViewController{
    
    UIImage *image;
    UIImagePickerController *imgPicker;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(100, 200, 200, 100)];
    button.backgroundColor = [UIColor whiteColor];
    [button setTitle:@"Attach" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    imgPicker = [[UIImagePickerController alloc]init];
    imgPicker.delegate = self;
    imgPicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imgPicker.mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage,nil];
//    [self presentViewController:imgPicker animated:YES completion:nil];

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if ([[info valueForKey:UIImagePickerControllerMediaType]isEqualToString:@"public.image"]) {
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(20, 350, 200, 200);
    [self.view addSubview:imageView];
    
    [imgPicker dismissViewControllerAnimated:YES completion:nil];

    
   
}
    -(void)buttonClicked{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!!!" message:@"Select:" preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Browse" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
             [imgPicker dismissViewControllerAnimated:YES completion:nil];
            [self presentViewController:imgPicker animated:YES completion:nil];
        }];
       
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
        }];
        
        [alert addAction:action1];
        [alert addAction:action2];
        [alert addAction:action3];
        
        
        [self presentViewController: alert animated:YES completion:^{
            
        }];
        
        
    }

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [imgPicker dismissViewControllerAnimated:YES completion:nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    }

@end
